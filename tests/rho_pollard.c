#include <stdio.h>
#include "bignum.h"

unsigned long int gcd(unsigned long int a, unsigned long int b){
  unsigned long int old_r, r = 1;
  if(a < b)
    return gcd(b,a);

  while(r != 0){
    old_r = r;
    r = a % b;
    a = b;
    b = r;
  }

  return old_r;
}

unsigned long int f(unsigned long x){
  return x*x + 1;
}

unsigned long int Pollard(unsigned long n){
  unsigned long int x = 2l;
  unsigned long int y = 2l;
  unsigned long int d = 1l;
  while(d==1l){
    x = f(x)%n;
    y = f(f(y))%n;
    d = gcd(x-y, n);
  }
  return d;
}


bignum_t Pollard2(bignum_t n){
  bignum_t f2(bignum_t x){
    return bignum_pow(x,2) + 1;
  }
  unsigned long int x1 = 2l;
  unsigned long int y2 = 2l;
  unsigned long int d3 = 1l;

  bignum_t x = bignum_from_int(x1);
  bignum_t y = bignum_from_int(y2);
  bignum_t d = bignum_from_int(d3);

  while(d == bignum_from_int(1l)){
    x = bignum_mod(f2(x),n);
    y = bignum_mod(f2(f2(y)),n);
    d = gcd(x-y, n);
  }
  return d;
}


int main() {
  // En utilisant l'algorithme rho de Pollard, factorisez les entiers suivants

  unsigned long int n1 = 17 * 113;

  unsigned long int n2 = 239 * 431;
  unsigned long int n3 = 3469 * 4363;
  unsigned long int n4 = 15241 * 18119;

  unsigned long int n5 = 366127l * 416797l;
  unsigned long int n6 = 15651941l * 15485863l;

  bignum_t n7, n8;

  n7 = bignum_mul(bignum_sub(bignum_pow(bignum_from_int(2), bignum_from_int(127)),
                             bignum_from_int(1)),
                  bignum_sub(bignum_pow(bignum_from_int(2), bignum_from_int(61)),
                             bignum_from_int(1)));

  n8 = bignum_mul(bignum_sub(bignum_pow(bignum_from_int(2), bignum_from_int(607)),
                             bignum_from_int(1)),
                  bignum_sub(bignum_pow(bignum_from_int(2), bignum_from_int(2203)),
                             bignum_from_int(1)));



  printf("PGCD(42,24) = %lu\n", gcd(42,24));
  printf("PGCD(42,24) = %s\n", bignum_to_str(bignum_gcd(bignum_from_int(42),bignum_from_int(24))));
  printf("Pollard n1 = %ld\n", Pollard(n1));
  printf("Pollard n2 = %ld\n", Pollard(n2));
  printf("Pollard n3 = %ld\n", Pollard(n3));
  printf("Pollard n4 = %ld\n", Pollard(n4));
  printf("Pollard n5 = %ld\n", Pollard(n5));
  printf("Pollard n6 = %ld\n", Pollard(n6));
  printf("Pollard n7 = %d\n", bignum_to_str(Pollard2(n7)));
  printf("Pollard n8 = %ld\n", Pollard(n8));
  return 0;
}
